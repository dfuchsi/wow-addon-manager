#!/bin/zsh
#
#    wow-addon-manager, a World of Warcraft addon manager
#    Copyright (C) 2018  Daniel Müller (perlfuchsi@gmail.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

ADDONS_LIST="${HOME}/wow_addons/addons.txt"
ADDONS_DIR="${HOME}/wow_addons/addons"
WOW_DIR="/mnt/storage/data/installed_games/World of Warcraft"
TMP_DIR="/tmp"
GREP="rg"

cd "$ADDONS_DIR"

T="${TMP_DIR}/wow_addons"

if [ ! -e "$T" ]; then
    mkdir -p "$T"
fi

addons=0
checked=1
installed=0

update_all() {
    addons=`cat ${ADDONS_LIST} | wc -l`
    for url in `cat $ADDONS_LIST`; do
        fetch $url
    done
}

install() {    
    if [ -z `$GREP "$1" ${ADDONS_LIST}` ]; then
        fetch "$1"
        echo "$1" >> "${ADDONS_LIST}"
    else
        echo "already installed"
    fi
}

list() {
    cat "${ADDONS_LIST}"
}

usage() {
    echo "usage `basename $0` update|install|list [\$url]"
    exit 1
}

_install() {
    filename="$1"
    AT="${T}/$(basename -s .zip $filename)"    
    unzip -o -q "$filename" -d "$AT"

    if [ $? != 0 ]; then
        echo "unpack failed: $?"
        return 1
    fi

    for file in `ls $AT`; do        
        src="${AT}/${file}"
        dst="${WOW_DIR}/interface/addons/${file}"
        if [ -d "$src" ]; then            
            if [ -d "$dst" ]; then
                rm -rf "$dst"
            fi
            mv "$src" "$dst"
        fi
    done

    return 0
}

fetch() {
    url="$1"

    echo "[${checked}/${addons}/${installed}] checking $url"

    # if the url is from curseforge or wowace, we can append "/files/latest"
    if [ `expr $url : "https://wow.curseforge.com/projects/"` != "0" ]; then
        url="${url%/files/latest}"
        url="${url}/files/latest"
    elif [ `expr $url : "https://www.wowace.com/projects/"` != "0" ]; then
        url="${url%/files/latest}"
        url="${url}/files/latest"
    fi

    wget --trust-server-names -c -o /tmp/wow-addons.log "${url}"
    filename=`rg -oxN -e "Saving to: ‘(.+)’" -r '$1' /tmp/wow-addons.log`
    if [ -n "$filename" -a -e "$filename" ]; then
        _install "$filename" 
        if [ $? = 0 ]; then
            ((installed++)) 
            echo "[${checked}/${addons}/${installed}] installed/updated $filename"
        else
            echo "install failed!"
        fi
    fi
    ((checked++))

}

case "$1" in
    update|u) update_all;;
    install|i)
        # this does not work in bash.
        ((addons=$#-2))
        for (( i=2; i <= $#; i++ )); do
            install $@[$i]
        done
        ;;
    list|l) list;;
    *) usage;;
esac

# cleanup log file
if [ -e /tmp/wow-addons.log ]; then
    rm -f /tmp/wow-addons.log
fi

